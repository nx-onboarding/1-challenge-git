function numberModifier(inputNumber) {
  /* ***
   *
   * You can only use divisions
   *
   * Return expected: 159
   *
   *
   *** */
  let i = 0;
  while (inputNumber / i !== 159) {
    i++;
  }
  return inputNumber / i;
}

module.exports = {
  numberModifier,
};