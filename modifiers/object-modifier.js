function sequentialnumber(number) {
  let numberToString = String(number);
  let lastNumber = numberToString.charAt(numberToString.length - 1);
  let listNumber = [Number(lastNumber) + 1];
  for (let index = 0; index < numberToString.length - 1; index++) {
    listNumber.push(listNumber[index] + 1);
  }
  return Number(listNumber.join(''));
}

function objectModifier(inputObject) {
  /* ***
   *
   * Return expected:
   *
   * {
   *  eman: "name: John",
   *  emaNtsal: "lastName: Doe",
   *  enohp: 5678,
   *  delbanEsi: false
   * }
   *
   *** */
  let objResult = {};
  let entries = Object.entries(inputObject);
  entries.map(([key, val] = entry) => {
    let reverseKey = key.split('').reverse().join('');
    if (typeof val === 'string') {
      objResult[reverseKey] = `${key}: ${val}`;
    }

    if (key === 'phone') {
      let numbers = sequentialnumber(val);
      objResult[reverseKey] = `${key}: ${numbers}`;
    }
    if (key === 'isEnabled') {
      objResult[reverseKey] = `${key}: ${!val}`;
    }
  });
  return objResult;
}

module.exports = {
  objectModifier,
};
