function stringModifier(inputString) {
  /* ***
   *
   * Return expected: tSrOiDcBaA
   *
   *** */
  let convertedText = [];
  inputString
    .split('')
    .sort()
    .forEach((letter, index) => {
      index = index + 1;
      if (index % 2 === 0) {
        convertedText.push(letter.toLowerCase());
      } else {
        convertedText.push(letter.toUpperCase());
      }
    });
  return convertedText.join('');
}

module.exports = {
  stringModifier,
};
