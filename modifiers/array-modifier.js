function arrayModifier(inputArray) {
  /* ***
   *
   * Return expected: ['paml', 'rouf', 'moor', 'lalh']
   *
   *** */

  let result = [];
  inputArray.forEach((work) => {
    let pair = [];
    let odd = [];
    work.split('').forEach((letter, index) => {
      index = index + 1;
      if (index % 2 === 0) {
        pair.push(letter);
      } else {
        odd.push(letter);
      }
    });
    result.push(pair.reverse().concat(odd.reverse()).join(''));
  });

  return result;
}

module.exports = {
  arrayModifier,
};
